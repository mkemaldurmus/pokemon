import sbtassembly.AssemblyPlugin.autoImport.assembly

ThisBuild / version := "1.0.0"

ThisBuild / scalaVersion := "2.13.8"

name := "pokemon"
assembly / mainClass := Some("com.kemal.Boot")
Compile / mainClass := Some("com.kemal.Boot")

val akkaHttpV = "10.2.9"
val circeV = "0.14.1"
val akkaHttpCirceV = "1.39.2"
val akkaV = "2.6.9"
val quillV = "3.12.0"
val mockitoV      = "3.4.6"
val scalaTestV    = "3.0.8"
val scalaLoggingV = "3.9.2"
val logbackV      = "1.2.3"



libraryDependencies ++= Seq(
  "ch.qos.logback"    % "logback-classic"   % logbackV,
  "com.typesafe.akka" %% "akka-actor-typed" % akkaV,
  "com.typesafe.akka" %% "akka-http" % akkaHttpV,
  "com.typesafe.akka" %% "akka-stream" % akkaV,
  "com.typesafe.akka" %% "akka-slf4j" % akkaV,
  "de.heikoseeberger" %% "akka-http-circe" % akkaHttpCirceV,
  "com.typesafe.akka" %% "akka-stream" % akkaV,
  "com.typesafe.akka"   %% "akka-http-spray-json" % akkaHttpV,

  "io.circe" %% "circe-generic" % circeV,
  "io.getquill" %% "quill-async-postgres" % quillV,
  "com.typesafe.scala-logging" %% "scala-logging"           % scalaLoggingV,
  "org.mockito"                 % "mockito-core"            % mockitoV   % Test,
  "org.scalatest"              %% "scalatest"               % scalaTestV % Test,
  "com.typesafe.akka"          %% "akka-http-testkit"       % akkaHttpV  % Test,
  "com.typesafe.akka"          %% "akka-stream-testkit"     % akkaV      % Test
)

assembly / assemblyMergeStrategy := {
  case PathList("application.conf")  => MergeStrategy.concat
  case PathList("reference.conf")    => MergeStrategy.concat
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case _                             => MergeStrategy.first
}

lazy val root = (project in file("."))
  .settings(
    name := "Pokemon"
  )

