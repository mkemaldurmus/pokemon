#!/bin/bash

export JAVA_OPTS=" ${JAVA_OPTS} -XX:+ExitOnOutOfMemoryError -XX:MaxRAMPercentage=65 -XshowSettings:vm "

# shellcheck disable=SC2086
exec java $JAVA_OPTS --add-opens java.base/jdk.internal.misc=ALL-UNNAMED \
    -Dio.netty.tryReflectionSetAccessible=true \
    -Dconfig.resource=application.conf \
    -jar /app/app.jar
