FROM openjdk:11

ARG JAR_PATH="pokemon*.jar"

ENV LANG "en_US.UTF-8"
ENV LC_ALL "en_US.UTF-8"
ENV TZ "Europe/Istanbul"

ENV JAVA_OPTS ""
ENV PROFILE ""

WORKDIR /app


COPY run.sh .
RUN chmod +x run.sh
EXPOSE 9000

COPY $JAR_PATH app.jar
ENTRYPOINT ["/app/run.sh"]
